//dependencies

var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema

var productSchema = new mongoose.Schema({
    title : String,
    image: String,
    content: String
});


//return model

module.exports = restful.model('Products', productSchema);